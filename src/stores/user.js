import { defineStore } from 'pinia';
import {
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  signOut,
  updateProfile,
} from 'firebase/auth';
import { firebaseAuth } from '../firebase/config';
import router from '@/router';

export const useUserStore = defineStore('user', {
  state: () => ({
    displayName: null,
    email: null,
    photoURL: null,
    status: 'not-authenticated', // ? checking, not-authenticated, authenticated
    uid: null,
  }),
  actions: {
    async registerUser(nombre, email, password) {
      this.status = 'checking';
      try {
        const { user } = await createUserWithEmailAndPassword(firebaseAuth, email, password);

        await updateProfile(firebaseAuth.currentUser, {
          displayName: nombre,
        });

        this.displayName = nombre;
        this.email = email;
        this.uid = user.uid;
        this.photoURL = user.photoURL;
        this.status = 'authenticated';

        router.push('/');
      } catch (error) {
        this.status = 'not-authenticated';
        console.log(error);
        alert(error.message);
      }
    },
    async loginUser(email, password) {
      this.status = 'checking';
      try {
        const { user } = await signInWithEmailAndPassword(firebaseAuth, email, password);

        this.displayName = user.displayName;
        this.email = user.email;
        this.uid = user.uid;
        this.photoURL = user.photoURL;
        this.status = 'authenticated';
        router.push('/');
      } catch (error) {
        this.status = 'not-authenticated';
        console.log(error);
        alert(error.message);
      }
    },
    async logout() {
      try {
        await signOut(firebaseAuth);
        this.displayName = null;
        this.email = null;
        this.photoURL = null;
        this.status = 'not-authenticated';
        this.uid = null;

        router.push('/login');
      } catch (error) {
        console.log(error);
        alert(error.message);
      }
    },
    checkUserAuth() {
      this.status = 'checking';
      return new Promise((resolve, reject) => {
        const unsubcribe = onAuthStateChanged(
          firebaseAuth,
          async (user) => {
            if (!user) {
              this.displayName = null;
              this.email = null;
              this.photoURL = null;
              this.status = 'not-authenticated';
              this.uid = null;
            } else {
              this.displayName = user.displayName;
              this.email = user.email;
              this.uid = user.uid;
              this.photoURL = user.photoURL;
              this.status = 'authenticated';
            }

            resolve(user);
          },
          (e) => reject(e),
        );

        unsubcribe();
      });
    },
  },
  getters: {
    isAuthenticating: (state) => state.status === 'checking',
  },
});
