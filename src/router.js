import { createRouter, createWebHistory } from 'vue-router';
import { useUserStore } from '@/stores/user';

const checkAuth = async (to, from, next) => {
  console.log({ to, from });
  const { checkUserAuth } = useUserStore();

  const user = await checkUserAuth();

  if (to.name === 'login' || to.name === 'register') {
    if (user) {
      next('/');
    } else {
      next();
    }
  } else {
    if (!user) {
      next('/login');
    } else {
      next();
    }
  }
};

const routes = [
  {
    path: '/',
    name: 'home',
    beforeEnter: checkAuth,
    component: () => import('@/pages/HomePage.vue'),
  },
  {
    path: '/login',
    name: 'login',
    beforeEnter: checkAuth,
    component: () => import('@/pages/LoginPage.vue'),
  },
  {
    path: '/register',
    name: 'register',
    beforeEnter: checkAuth,
    component: () => import('@/pages/RegisterPage.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    beforeEnter: checkAuth,
    component: () => import('@/pages/NotFoundPage.vue'),
  },
];

const history = createWebHistory(import.meta.env.BASE_URL);

const router = createRouter({
  history,
  linkActiveClass: 'active',
  routes,
});

export default router;
